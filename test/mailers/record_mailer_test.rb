require 'test_helper'

class RecordMailerTest < ActionMailer::TestCase
  test "send_records" do
    mail = RecordMailer.send_records
    assert_equal "Send records", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
