# Preview all emails at http://tutorial-rugia.c9.io/rails/mailers/record_mailer
class RecordMailerPreview < ActionMailer::Preview

  # Preview this email at http://tutorial-rugia.c9.io/rails/mailers/record_mailer/send_records
  def send_records
    RecordMailer.send_records('rugia813@yahoo.com.tw')
  end

end
