class CreateDailyRecords < ActiveRecord::Migration
  def change
    create_table :daily_records do |t|
      t.string :sleep_time
      t.string :restfulness
      t.text :study_progress
      t.datetime :date

      t.timestamps null: false
    end
  end
end
