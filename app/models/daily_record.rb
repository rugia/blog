class DailyRecord < ActiveRecord::Base
  validates :date, uniqueness: true
end
