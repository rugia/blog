class User < ActiveRecord::Base
  validates :name, presence: true, length: { minimum: 3 }, 
  uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 1 }
    
  has_secure_password
end
