class Article < ActiveRecord::Base
  validates :title, presence: true
  validate :picture_size
  has_many :comments, dependent: :destroy
  
  mount_uploader :picture, PictureUploader
  
  def picture_size
    if picture.size > 5.megabytes
      errors.add(:picture, "should be less than 5MB")
    end
  end
  
end
