class RecordMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.record_mailer.send_records.subject
  #
  def send_records(email)
    @daily_records = DailyRecord.all
    @email = email

    mail to: @email, subject: 'Daily Records'
  end
end
