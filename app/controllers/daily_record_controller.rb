class DailyRecordController < ApplicationController
  before_action :require_login
  def daily_record
    @memo ||= Memo.create
    @memo = Memo.find_by(id:1)
    
    cookies[:base_date] ||= Time.now.at_midnight
    @base_date = cookies[:base_date].to_datetime
    @day1 = DailyRecord.find_by(date: @base_date)
    @day1 ||= DailyRecord.create(date: @base_date)
    
    @day2 = DailyRecord.find_by(date: @base_date + 1.day)
    @day2 ||= DailyRecord.create(date: @base_date + 1.day)
    
    @day3 = DailyRecord.find_by(date: @base_date + 2.day)
    @day3 ||= DailyRecord.create(date: @base_date + 2.day)
    
    @day4 = DailyRecord.find_by(date: @base_date + 3.day)
    @day4 ||= DailyRecord.create(date: @base_date + 3.day)
    
    @day5 = DailyRecord.find_by(date: @base_date + 4.day)
    @day5 ||= DailyRecord.create(date: @base_date + 4.day)
    
    @day6 = DailyRecord.find_by(date: @base_date + 5.day)
    @day6 ||= DailyRecord.create(date: @base_date + 5.day)
    
    @day7 = DailyRecord.find_by(date: @base_date + 6.day)
    @day7 ||= DailyRecord.create(date: @base_date + 6.day)
    
  end
  
  def update
    @daily_record = DailyRecord.find_by(date: params[:daily_record][:base_date].to_datetime)
    if @daily_record.update(daily_record_params)
      flash[:success] = "#{@daily_record.date.to_date} Updated successfully!!"
      redirect_to daily_record_path
    end
  end
  
  def next
    cookies[:base_date] = cookies[:base_date].to_datetime + 6.day
    redirect_to daily_record_path
  end
  
  def previous
    cookies[:base_date] = cookies[:base_date].to_datetime - 6.day
    redirect_to daily_record_path
  end
  
  def send_mail
    RecordMailer.send_records(params[:email]).deliver_now
    redirect_to daily_record_path
  end
  
  private
  def require_login
    if current_user.nil?
      redirect_to articles_path
      flash[:warning]='You need to login first!'
    end
  end
  
  def daily_record_params
    params.require(:daily_record).permit(:sleep_time, :restfulness, :study_progress)
  end
end
