class SessionsController < ApplicationController
  def new
  end
  
  def create
    user = User.find_by(name: params[:session][:name])
    if user && user.authenticate(params[:session][:password])
      log_in(user)
      redirect_to articles_path
    else
      flash.now[:danger] = 'Invalid user name or password!'
      render 'new'
    end
  end
  
  def destroy
    log_out
    redirect_to articles_path
  end
end
