class ArticlesController < ApplicationController
  before_action :require_login, only: [:create, :edit, :update, :new, :destroy]
  
  def new
    @article = Article.new
  end
  
  def create
    @article = Article.new(article_params)
    if @article.save
      flash[:success] = 'Article created successfully!'
      redirect_to @article
    else
      render 'new'
    end
  end
  
  def show
    @article = Article.find(params[:id])
    #debugger
  end
  
  def index
    @articles = Article.all.reverse
  end
  
  def edit
    @article = Article.find(params[:id])
  end
  
  def update
    @article = Article.find(params[:id])
    if @article.update(article_params)
      flash[:success] = 'Article updated successfully!'
      redirect_to @article
    else
      render 'edit'
    end
  end
  
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    flash[:success] = 'Article deleted successfully!'
    redirect_to articles_path
  end
  
  def about
  end
    
  private
  
  def article_params
    params.require(:article).permit(:title, :content, :picture)
  end
  
  def require_login
    if current_user == nil
      flash[:alert]='You need to login to do that!'
      redirect_to articles_path
    end
  end
end
