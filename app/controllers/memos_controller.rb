class MemosController < ApplicationController
  def update
    d = Memo.first
    d.text = params[:memo][:text]
    d.save
    flash[:success]="Memo saved!"
    redirect_to daily_record_path
  end
  
  private
  
  def memo_params
    params.require(:memo).permit(:text)
  end
end
